package main

import (
	"bufio"
	"fmt"
	"golang.org/x/crypto/ssh"
	"log"
	"os"
	"time"
)

// readLines reads a whole file into memory and returns a slice of its lines.
func readLines(path string) ([]string, error) {
	file, err := os.Open(path)
	if err != nil {
		log.Println(err)
	}
	defer file.Close()
	var lines []string
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		lines = append(lines, scanner.Text())
	}
	return lines, scanner.Err()
}

func parameters(ips, users, pass []string) {
	for _, ip := range ips {
		for _, u := range users {
			for _, p := range pass {
				client, session, err := connectToHost(u, ip, p)
				if err != nil {
					fmt.Println(" não foi possivel conectar no host: ", ip, u)
					continue
				}
				if client == nil {
					fmt.Println("invalide Password", ip, u, p)
				} else {
					fmt.Println("Conexão OK", ip, u, p)
          break
				}
				fmt.Println(session)
			}
		}
	}
	return
}

func connectToHost(user, host, pass string) (*ssh.Client, *ssh.Session, error) {
	sshConfig := &ssh.ClientConfig{
		User: user,
		Auth: []ssh.AuthMethod{ssh.Password(pass)},
		Timeout: time.Duration(2)  * time.Second,
	}
	client, err := ssh.Dial("tcp", host, sshConfig)
	if err != nil {
		return nil, nil, err
	}
	session, err := client.NewSession()
	if err != nil {
		client.Close()
		return nil, nil, err
	}
	defer session.Close()
	return client, session, nil
}

func main() {
	ips, err := readLines("ips.txt")
	users, err := readLines("users.txt")
	pass, err := readLines("passwords.txt")
	if err != nil {
		panic(err)
	}
	parameters(ips, users, pass)
	//fmt.Println(client, session)
}
