package mainok
import (
    "os"
    "bufio"
    "bytes"
    "io"
    "fmt"
 
)
// Read a whole file into the memory and store it as array of lines
func readLines(path string) (lines []string, err error) {
    var (
        file *os.File
        part []byte
        prefix bool
    )
    if file, err = os.Open(path); err != nil {
        panic(err)
    }
    defer file.Close()
    reader := bufio.NewReader(file)
    buffer := bytes.NewBuffer(make([]byte, 0))
    for {
        if part, prefix, err = reader.ReadLine(); err != nil {
            break
        }
        buffer.Write(part)
        if !prefix {
            lines = append(lines, buffer.String())
            buffer.Reset()
        }
    }
    if err == io.EOF {
        err = nil
    }
    return
}

func main() {
    lines, err := readLines("sherlock.txt")
    if err != nil {
        panic(err)        
    }
    for _, line := range lines {
        fmt.Println(line)
    }
 
}